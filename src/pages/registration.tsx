/* eslint-disable linebreak-style */

import { AuthLayout } from '@components/layout/auth-layout';
import { SEO } from '@components/common/seo';
import { RegistrationMain } from '@components/registration/registration-main'; // Update the import path accordingly
import type { ReactElement, ReactNode } from 'react';

const Registration = (): JSX.Element => {
  return (
    <div className='grid min-h-screen grid-rows-[1fr,auto]'>
      <SEO
        title='Twistify - Sign Up'
        description='Create a Twistify account to stay connected with what’s happening.'
      />
      <RegistrationMain />
    </div>
  );
};

Registration.getLayout = (page: ReactElement): ReactNode => (
  <AuthLayout>{page}</AuthLayout>
);

export default Registration;
