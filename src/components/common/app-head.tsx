import Head from 'next/head';

export function AppHead(): JSX.Element {
  return (
    <Head>
      <title>Twistify</title>
      <meta name='og:title' content='Twistify' />
      <link rel='icon' href='/favicon.ico' />
      <link rel='manifest' href='/site.webmanifest' key='site-manifest' />
      <meta name='Twistify:site' content='@ccrsxx' />
      <meta name='Twistify:card' content='summary_large_image' />
    </Head>
  );
}
